<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'TodoController@index');

//Route::get('/todos/create', 'TodoController@create');
//Route::post('/todos','TodoController@store');
//Route::get('todos/{todo}/edit', 'TodoController@edit');
//Route::put('todos/{todo}', 'TodoController@update');
//Route::get('/todos/{todo}/delete', 'TodoController@delete');

Route::get('/todos','TodoController2@index');
Route::get('/todos/{id}','TodoController2@show');
Route::post('/todos/store','TodoController2@store');
Route::post('/todos/update/{id}','TodoController2@update');
Route::post('/todos/delete/{id}','TodoController2@destroy');