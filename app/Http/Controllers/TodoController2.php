<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TodoController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
{
    $data = \App\Todo::all();
    $res= $data;
    return response($res);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->json()->all();
        $name = $request->input("name");

        $data = new \App\Todo();
        $data->name = $name;
        if ($data->save()) {
            return response(null, 200);
        }
        else {
            return response(null, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /*public function show($id)
    {
        $data = \App\Todo::where('id',$id)->get();
    
        if(count($data) > 0){ //mengecek apakah data kosong atau tidak
            $res['message'] = "Success!";
            $res['values'] = $data;
            return response($res);
        }
        else{
            $res['message'] = "Failed!";
            return response($res);
        }
    }
    */

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $req = $request->json()->all();
        $name = $request->input("name");
        
        
        $data = \App\Todo::find($id);
        $data->name = $name;
        

        if ($data->save()) {
            return response(null, 200);
        }
        else {
            return response(null, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = \App\Todo::where('id',$id)->first();

        if($data->delete()){
        $res['message'] = "Success!";
        $res['value'] = "$data";
        return response($res);
        }
        else{
        $res['message'] = "Failed!";
        return response($res);
        }
    }
}
